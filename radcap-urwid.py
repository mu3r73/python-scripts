#!/usr/bin/env python3

import urwid
import json
import os
import random
import shutil
import sys
import urllib.request
import re


# global vars
menu_choice = None


def main():
  # decide what to do
  args = sys.argv[1:len(sys.argv)]
  process_args(args)
  

def print_syntax():
  script_name = os.path.basename(sys.argv[0])
  print('radcap, a selector for radio caprice stations')
  print('usage:')
  print(f'  python {script_name} [<subgenre_search_string>] [options] [suboptions]')
  print('options:')
  print('  --help: show this help screen')
  print('  --menu: show genre/subgenre menu')
  print('  --rnd: play random station')
  print('  --genre [NUMBER]: choose a genre by number')
  print('suboptions:')
  print('  --subgenre [NUMBER]: choose a subgenre by number (requires: --genre)')
  print('examples:')
  print(f'  {script_name} --menu                    asks which genre / subgenre to play')
  print(f'  {script_name} --rnd                     plays a random station')
  print(f'  {script_name} jazz                      asks which *jazz* station to play')
  print(f'  {script_name} \'sludge metal\'            plays the "sludge metal" station')
  print(f'  {script_name} \'death metal\' --rnd       plays a random *death metal* station')
  print(f'  {script_name} --genre 7 --subgenre 46   plays genre 7, subgenre 46')
  print(f'  {script_name} --genre 1                 asks which genre 1 subgenre to play')
  print(f'  {script_name} --genre 6 --rnd           plays a random genre 6 station')
  print('requires: mpv, urwid (for python 3)')


def download_json_file_if_missing(url, file_name):
  if not os.path.exists(file_name):
    print(file_name, "file doesn't exist")
    download_json_file(url, file_name)


def download_json_file(url, file_name):
  print('downloading', url, '...')
  with urllib.request.urlopen(url) as response:
    json_file = open(file_name, "w")
    json_file.write(response.read().decode('utf-8'))
    json_file.close()


def load_from_local_file(file_name):
  print('loading & parsing file:', file_name, '...')
  with open(file_name) as json_file:
    json_data = json.load(json_file)
  return json_data


def process_args(args):
  if not args or args[0] == '--help':
    print_syntax()
    return
  
  # load stations data
  local_source = f'{os.path.dirname(sys.argv[0])}/radcap.json'
  web_source = 'https://gist.githubusercontent.com/leejunip/741d5a8330262b73b53eec676e44ec70/raw/61ad5fbc1332a1708c2eed75c417b718e81d4852/radcap.json'

  download_json_file_if_missing(web_source, local_source)
  json_data = load_from_local_file(local_source)
  
  if args[0] == '--menu':
    show_choose_genre_menu(json_data)
    return
  
  if args[0] == '--rnd':
    play_random_station(json_data, '(random)')
    return
  
  if args[0] == '--genre':
    process_subgenre(json_data, args[1], args[2:len(args)])
    return

  # verify that args[0] doesn't start with --
  if not_an_option(args[0]):
    play_station_matching(json_data, args)
    return

  # none of the above => syntax error
  print(f'syntax error: {args[0]} is not an option')


def not_an_option(arg):
  option = r'^\-\-'
  m = re.match(option, arg)
  return not m


def process_subgenre(json_data, genre, args):
  try:
    genres = get_genres(json_data)
    genre_num = int(genre)
    genre_str = genres[genre_num]
  except:
    print(f'syntax error: genre should be a NUMBER between 0 and {len(genres) - 1}')
    return
    
  if not args:
    show_choose_subgenre_menu(json_data, genre_str)
    return
  
  if args[0] == '--rnd':
    play_random_station(json_data, genre_str)
    return
  
  if args[0] == '--subgenre':
    play_station(json_data, genre_str, args[1])
    return
  
  # none of the above => syntax error
  print(f'syntax error: {args[0]} is not a suboption')
  

def show_choose_genre_menu(json_data):
  genres_names = get_genres(json_data)
  
  menu = urwid.Padding(mk_menu('Genre:', genres_names), left = 2, right = 2)
  top = urwid.Overlay(
          menu,
          urwid.SolidFill(u'\N{MEDIUM SHADE}'),
          align='center',
          width=('relative', 60),
          valign='middle',
          height=('relative', 60),
          min_width=20,
          min_height=9)
  urwid.MainLoop(top, palette=[('reversed', 'standout', '')], unhandled_input=exit_main).run()
  
  # a genre was chosen via menu,
  # and is now stored in the global variable menu_choice
  show_choose_subgenre_menu(json_data, menu_choice)


def show_choose_subgenre_menu(json_data, genre_str):
  if genre_str == '(random)':
    genre_str = choose_random_genre(json_data)
  
  subgenres = get_subgenres(json_data, genre_str)
  subgenres_names = get_subgenres_names(subgenres)

  menu = urwid.Padding(mk_menu(f'{genre_str} Subgenre:', subgenres_names), left = 2, right = 2)
  top = urwid.Overlay(
          menu,
          urwid.SolidFill(u'\N{MEDIUM SHADE}'),
          align='center',
          width=('relative', 60),
          valign='middle',
          height=('relative', 60),
          min_width=20,
          min_height=9)
  urwid.MainLoop(top, palette=[('reversed', 'standout', '')], unhandled_input=exit_main).run()
  
  # a genre was chosen via menu,
  # and is now stored in the global variable menu_choice
  subgenre_str = menu_choice
  
  if subgenre_str == '(random)':
    play_random_station(subgenres)
    return
  
  # after closing the menu
  chosen = [ sg for sg in subgenres if sg.get('name') == subgenre_str ]
  if not chosen:
   	print('error: no subgenre was chosen')
   	return

  # a specific subgenre was chosen
  subgenre = chosen[0]
  play_radio(subgenre.get('url'), subgenre.get('name'), genre_str)


def play_random_station(json_data, genre_str = '(random)'):
  if not is_whole_data(json_data):
    subgenres = json_data
  else:
    if (genre_str == '(random)'):
      genre_str = choose_random_genre(json_data)
    subgenres = get_subgenres(json_data, genre_str)

  station = choose_random_subgenre(subgenres)
  station_name = station.get('name')
  station_url = station.get('url')
  
  play_radio(station_url, station_name, genre_str)


def is_whole_data(json_data):
  return (type(json_data) == dict) and json_data.get('BLUES / FUNK / SOUL')


def play_station_matching(json_data, args):
  pattern_str = args[0]

  all_subgenres = get_ALL_subgenres(json_data)
  matching_subgenres = get_matching_subgenres(all_subgenres, pattern_str)
  
  # no matching subgenres
  if not matching_subgenres:
    print(f'no matching stations for pattern = {pattern_str}')
    return
  
  # if only 1 matching subgenre
  if len(matching_subgenres) == 1:
    station = matching_subgenres[0]
    play_radio(station.get('url'), station.get('name'), station.get('genre'))
    return
  
  # more than 1 matching subgenres
  
  # if --rnd argument is present
  if len(args) > 1 and args[1] == '--rnd':
    station = random.choice(matching_subgenres)
    play_radio(station.get('url'), station.get('name'), station.get('genre'))
    return

  # more than 1 matching subgenres, not random

  menu = urwid.Padding(mk_menu('Matching subgenres:', get_subgenres_names(matching_subgenres)), left = 2, right = 2)
  top = urwid.Overlay(
          menu,
          urwid.SolidFill(u'\N{MEDIUM SHADE}'),
          align='center',
          width=('relative', 60),
          valign='middle',
          height=('relative', 60),
          min_width=20,
          min_height=9)
  urwid.MainLoop(top, palette=[('reversed', 'standout', '')], unhandled_input=exit_main).run()
  
  # a genre was chosen via menu,
  # and is now stored in the global variable menu_choice
  subgenre_str = menu_choice
  
  if subgenre_str == '(random)':
    play_random_station(matching_subgenres)
    return
  
	# after closing the menu
  chosen = [ sg for sg in matching_subgenres if sg.get('name') == subgenre_str ]
  if not chosen:
   	print('error: no subgenre was chosen')
   	return

  # a specific subgenre was chosen
  subgenre = chosen[0]
  play_radio(subgenre.get('url'), subgenre.get('name'), subgenre.get('genre'))


def play_station(json_data, genre_str, subgenre):
  try:
    subgenres = get_subgenres(json_data, genre_str)
    subgenre_num = int(subgenre)
    station = subgenres[subgenre_num]
  except:
    print(f'syntax error: subgenre should be a NUMBER between 0 and {len(subgenres) - 1}')
    return
    
  station_name = station.get('name')
  station_url = station.get('url')
  play_radio(station_url, station_name, genre_str)


def mk_menu(title, choices):
  body = [urwid.Text(title), urwid.Divider()]
  for c in choices:
    button = urwid.Button(c)
    urwid.connect_signal(button, 'click', on_menu_item_chosen, c)
    body.append(urwid.AttrMap(button, None, focus_map='reversed'))
  return urwid.ListBox(urwid.SimpleFocusListWalker(body))

def on_menu_item_chosen(button, choice):
  global menu_choice
  menu_choice = choice
  raise urwid.ExitMainLoop()


def exit_main(key):
  raise urwid.ExitMainLoop()


def choose_random_genre(json_data):
  genres = get_genres(json_data)

  genre_str = '(random)'
  while genre_str == '(random)':
    genre_str = random.choice(genres)
  
  return genre_str


def choose_random_subgenre(subgenres):
  return random.choice(subgenres)
  

def get_genres(json_data):
  genres = sorted(json_data.keys())
  genres.insert(0, '(random)')
  return genres


def get_subgenres(json_data, genre_str):
  return json_data.get(genre_str)


def get_subgenres_names(subgenres):
  subgenres_names = sorted([sg.get('name') for sg in subgenres])
  subgenres_names.insert(0, '(random)')
  return subgenres_names


def get_ALL_subgenres(json_data):
  genres = sorted(json_data.keys())
  subgenres = [ dict(sg, **{ 'genre': genre }) for genre in genres for sg in json_data.get(genre) ]
  return subgenres


def get_matching_subgenres(subgenres, pattern_str):
  pattern_str = pattern_str.replace('*', '.*').replace('..', '.').lower()
  matching = [ sg for sg in subgenres if re.search(pattern_str, sg.get('name').lower()) ]
  return matching


def play_radio(url, name, genre_str = None):
  playing_str = f'playing station: {name}'
  if genre_str:
    playing_str += f' [{genre_str}]'
  print(playing_str)
  print(url)
  try:
    proc = os.spawnlp(os.P_WAIT, 'mpv', 'mpv', url)
    proc.communicate()
  except:
    if (shutil.which('mpv') == None):
      print('error: mpv not installed')
      sys.exit()
    else:
      pass


if __name__ == "__main__":
  main()
