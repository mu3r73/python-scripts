Tested on arch / artix / manjaro linux & wine-staging (v5.0-1), Feb 2020.

WARNING: don't install in the same prefix as MS Speech Platform, or it won't work


1- create a 32-bit wine prefix and install speechsdk & vb runtime 2010+

$ WINEPREFIX=~/wine/sapi WINEARCH=win32 winetricks speechsdk
$ WINEPREFIX=~/wine/sapi winetricks vcrun2015


2- download balabolka cli (balcon) from:
http://www.cross-plus-a.com/bconsole.htm


3- extract balcon to the same wine prefix:

$ unzip balcon.zip -x ~/wine/sapi/drive_c/balcon


4- test that speechsdk installed ok:

$ WINEPREFIX=~/wine/sapi wine c:/balcon/balcon -l

... if everything's ok, the output should include:

<blah>
SAPI 5:
  Microsoft Mary
  Microsoft Mike
  Microsoft Sam
  SampleTTSVoice

... if not, try installing mono & gecko via winetricks or whatever, then repeat step 4


5- test for sound output:

$ WINEPREFIX=~/wine/sapi wine c:/balcon/balcon.exe -n "Mary" -t "hello world"

... if no sound, check alsa/pulse config & possibly missing lib32* stuff


6- (optional) download & install some high quality voices (Ivona, NextUp, &c)
