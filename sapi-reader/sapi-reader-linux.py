#!/usr/bin/env python3

import argparse
import os
import random
import re
import subprocess
import sys
import termios
import textwrap
import _thread
import time
import tty

try:
  import dictionaries
except ImportError:
  import dictionaries_stub as dictionaries


# constants
DEFAULT_SPEED = '3'
DEFAULT_LANG = 'en'
DEFAULT_VOICES = [ 'Joey', ]
VALID_LANGS = [ 'en', 'es', ]
VALID_VOICES = {
  'en': [ 'Joey', 'Kendra', ],
  'es': [ 'Miguel', 'Penélope', ],
}
MIN_SPEED = 0
MAX_SPEED = 10
SIMPLIFY_YEARS_LANGS = [ 'en', ]
PREPARE_LINES_LANGS = [ 'en', ]
KEEP_APOSTROPHE_LANGS = [ 'en', ]
SYMBOL_NAMES = {
  '.': {
    'en': 'dot',
    'es': 'punto',
  },
  '/': {
    'en': 'slash',
    'es': 'barra',
  }
}
MUTE = False
WRAP_COL = 50
PRIORITY_KEYS = 'cCfFsS'
# balcon.exe stopped being compatible after wine 8.1
# Error: OLE error 80045004 
# while this balcon issue isn't fixed, use wine 8.1
#WINE_BINARY_PATH = '/usr/bin/wine'
WINE_BINARY_PATH = f'{os.environ["HOME"]}/bin/wine-sapi/wine'
WINE_PREFIX = f'{os.environ["HOME"]}/wine/sapi'
BALCON_PATH = 'c:/balcon/balcon.exe'
SLEEP_TIME = .1
PARAGRAPH_PAUSE = .75


# global vars
g_parser = None
g_args = None
g_voices = DEFAULT_VOICES
g_voice = None
g_speed = DEFAULT_SPEED
g_lang = DEFAULT_LANG
g_simplify_years = False
g_prepare_line = False
g_lines = None
g_current_line = -1
g_keypresses = []
g_eof = False
g_fd = None
g_old_settings = None


def main():
  define_args()
  # decide what to do
  process_args()


def define_args():
  global g_parser
  global g_args
  g_parser = argparse.ArgumentParser(description='sapi-reader, a CLI wrapper for sapi + balcon')
  g_parser.add_argument('file_name', nargs='?', help='text file name')
  g_parser.add_argument('--voice', nargs='?', help='voice name')
  g_parser.add_argument('--speed', nargs='?', help='reading speed')
  g_parser.add_argument('--lang', nargs='?', help='text language')
  g_parser.add_argument('--text', nargs='?', help='read text argument instead of file')
  g_args = g_parser.parse_args()


def process_args():
  # if any of the following fail, they show related help and exit
  check_if_no_arguments()
  process_speed_arg()
  process_lang_arg()
  process_voice_arg()
  process_text_arg()
  process_file_name_arg()
  # here all arguments should be correct
  display_ui()


def check_if_no_arguments():
  args_values = list(vars(g_args).values())
  if not any(args_values):
    print_syntax()
    sys.exit()


def process_speed_arg():
  global g_speed
  
  g_speed = g_args.speed or g_speed
  if not int(g_speed) in range(MIN_SPEED, MAX_SPEED):
    print(f'invalid speed - options: a number between {MIN_SPEED} and {MAX_SPEED}')
    sys.exit()
  print('using speed:', g_speed)


def process_lang_arg():
  global g_lang
  global g_simplify_years
  global g_prepare_line
  
  g_lang = g_args.lang or g_lang
  if not g_lang in VALID_LANGS:
    print(f'invalid language - options: {", ".join(VALID_LANGS)}')
    sys.exit()
  if g_lang in SIMPLIFY_YEARS_LANGS:
    g_simplify_years = True
  if g_lang in PREPARE_LINES_LANGS:
    g_prepare_line = True
  print('using language:', g_lang)


def process_voice_arg():
  global g_voice
  
  g_voice = g_args.voice or random.choice(g_voices)
  if not is_valid_voice(g_voice, g_lang):
    print(f'invalid voice - options for "{g_lang}": {", ".join(VALID_VOICES.get(lang_code))}')
    sys.exit()
  print('using voice:', g_voice)


def is_valid_voice(voice_name, lang_code):
  return voice_name in VALID_VOICES.get(lang_code)


def process_text_arg():
  if g_args.text:
    read_line(g_args.text, True)
    sys.exit()


def process_file_name_arg():
  global g_lines
  
  if not g_args.file_name:
    print('a file name is required')
    return
  g_lines = load_file(g_args.file_name)  


def print_syntax():
  g_parser.print_help()
  script_name = os.path.basename(sys.argv[0])
  print('examples:')
  print(f'  {script_name} example.txt')
  print('      loads the example.txt file for reading with the default voice')
  print(f'  {script_name} --voice Sam book.txt')
  print('      loads the book.txt file for reading with the "Sam" voice')
  print(f'  {script_name} --voice Mary --text "hello world"')
  print('      reads "hello world" with the "Mary" voice')
  print('requires: wine (32-bit prefix), vcrun2015, speechsdk, balcon, [ivona]')


def print_ui_help():
  print()
  print('keys:')
  print('  spacebar: start/pause auto-reading')
  print('  ↓: read next paragraph')
  print('  ↑: read previous paragraph')
  print('  r: repeat latest paragraph')
  print('  s: stop reading paragraph')
  print('  c: clear the screen')
  print('  f: flush keyboard buffer')
  print('  q: quit')
  print()


def load_file(file_name):
  print('loading file:', file_name, '...')
  with open(file_name, 'r') as f:
    lines = f.readlines()

  if lines:
    lines = [ line for line in lines if len(line) > 1 ]
  lines.append('<EOF>')

  return lines


def display_ui():
  global g_current_line

  print_ui_help()

  setup_term()

  # watch for keypresses, non-blocking
  _thread.start_new_thread(watch_keyboard_loop, ())

  g_current_line = -1

  while True:
    c = getch()
    process_input(c)
    time.sleep(SLEEP_TIME) 


def process_input(key):
  if not key:
    return

  #print(ord(key), key)

  # C -> clear the screen
  if key in 'cC':
    clear_screen()
    return

  # F -> clear (flush) keyboard buffer
  if key in 'fF':
    clear_keyboard_buffer()
    return

  # Q -> quit
  if key in 'qQ':
    reset_term()
    sys.exit()

  # S -> stop reading
  if key in 'sS':
    stop_sapi_read()
    return

  # space -> read paragraphs until space is hit again
  if ord(key) == 32:
    read_loop()

  # down arrow -> read next paragraph
  elif ord(key) == 66:
    read_next()

  # up arrow -> read prev paragraph
  elif ord(key) == 65:
    read_prev()

  # r = re-read current paragraph
  elif key in 'rR':
    read_current(False)


def read_loop():
  while True:
    read_next()

    # end of file -> stop reading
    if g_eof:
      break

    # was a key pressed?
    c = None
    if kbhit():
      c = getch()
    
    # space, F or Q -> stop reading
    if not (c is None) and ((ord(c) == 32) or (c in 'fFqQ')):
      break


def read_next():
  global g_eof
  global g_current_line
  g_current_line += 1
  if g_current_line >= len(g_lines):
    g_eof = True
    g_current_line = len(g_lines) - 1
    return
  read_current()


def read_prev():
  global g_current_line
  g_current_line -= 1
  if g_current_line < 0:
    g_current_line = 0
    return
  read_current()


def read_current(show = True):
  line = g_lines[g_current_line]
  read_line(line, show)


def read_line(line, show):
  line = line.strip()
  if not line:
    return

  if show:
    print_line(line)

  if MUTE:
    return
  
  if line == '<PAUSE>':
    # pause reading
    putc(' ')

  prepared_line = prepare_line(line)
  if not prepared_line:
    return

  try:
    sapi_read_line(prepared_line)
    time.sleep(PARAGRAPH_PAUSE)

  except Exception as e:
    print(e)


def print_line(line):
  for l in textwrap.wrap(line, WRAP_COL):
    print(f'\r{l}')
  print('\r')


def prepare_line(line):
  if g_simplify_years:
    line = simplify_years(line)

  if g_prepare_line:
    line = dictionaries.prepare_line(line)
  else:
    line = line.lower()

  line = mute_punctuation(line)

  return line


def simplify_years(line):
  # year = 4 digits
  r = r'\b(\d{4})\b'
  return re.sub(r, simplify_year, line)


def simplify_year(year_str):
  y = year_str.group()
  return f'{y[:2]} {y[2:]}'


def mute_punctuation(line):
  p_replace = {
    '*': '',
    '"': '',
    './': f'{get_symbol_name(".", g_lang)} {get_symbol_name("/", g_lang)} ', # default: long speech-pause
    '/': get_symbol_name("/", g_lang), # default: long speech-pause
    "'.": '.', # default: apostrophe
    "',": ',', # default: apostrophe
  }
  for key, val in p_replace.items():
    line = line.replace(key, val)
  
  if not g_lang in KEEP_APOSTROPHE_LANGS:
    line = line.replace("'", ' ')
    line = re.sub(r'---', '', line)
  
  return line


def get_symbol_name(symbol, lang):
  return SYMBOL_NAMES.get(symbol).get(lang)


# replace this with a call to aws polly,
# if you're patient enough for dealing with tokens
def sapi_read_line(line):
  try:
    env = os.environ.copy()
    env['WINEPREFIX'] = WINE_PREFIX
    env['WINEDEBUG'] = '-all'
    subprocess.Popen(
      [ WINE_BINARY_PATH, BALCON_PATH, '-n', g_voice, '-s', g_speed, '-t', line ],
      stdout = None, stderr = None,
      env = env).wait()

  except Exception as e:
    print(e)


def stop_sapi_read():
  try:
    env = os.environ.copy()
    env['WINEPREFIX'] = WINE_PREFIX
    env['WINEDEBUG'] = '-all'
    subprocess.Popen(
      [ WINE_BINARY_PATH, BALCON_PATH, '-k' ],
      stdout = None, stderr = None,
      env = env).wait()

  except Exception as e:
    print(e)


def watch_keyboard_loop():
  while True:
    watch_keyboard()
    time.sleep(SLEEP_TIME)


def watch_keyboard():
  global g_keypresses

  try:
    key = sys.stdin.read(1)
    while ord(key) in (27, 79, 91):
      key = sys.stdin.read(1)

    if key in PRIORITY_KEYS:
      process_input(key)

    else:
      g_keypresses.append(key)

  except Exception as e:
    print(e)


def clear_keyboard_buffer():
  global g_keypresses
  termios.tcflush(sys.stdin, termios.TCIFLUSH)
  g_keypresses = []


def setup_term():
  global g_fd
  global g_old_settings

  g_fd = sys.stdin.fileno()
  g_old_settings = termios.tcgetattr(g_fd)
  tty.setraw(sys.stdin.fileno())


def reset_term():
  termios.tcsetattr(g_fd, termios.TCSADRAIN, g_old_settings)


def clear_screen():
  os.system('clear')


def getch():
  global g_keypresses

  if not kbhit():
    return None

  ch = g_keypresses.pop(0)

  return ch


def kbhit():
  return g_keypresses != []


def putc(key):
  global g_keypresses
  
  g_keypresses.insert(0, ' ')


if __name__ == '__main__':
  main()
