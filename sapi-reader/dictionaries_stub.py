import string
import re


# ivona: replace only when followed by punctuation
# prevent some expansions - eg, 'sun.' to 'sunday', 'no.' to 'number'
# preserve speech-pause after expanded words (punctuation eaten by the expansion)
l_space_before_punctuation = [
  #'cal', # default: california / calories  (it expands regardless => see d_replace_words_re)
  'corp', # default: corporation
  #'g', # default: grams  (it expands regardless => see d_replace_words_after_re)
  'in', # default: inches
  #'m', # default: meters (it expands regardless => see d_replace_words_re)
  'no', # default: number
  'sun', # default: sunday
  #'tm', # default: trademark  (it expands regardless => see d_replace_words_re)
  'wash', # default: washington
]

# ivona: replace whole words
d_replace_words_re = {
  'cal': 'cahl', # default: 'california' / 'calories'
  'sec': 'sehk', # default: 'seconds'
  'tm': 'T M', # default: 'trademark'
}

# various fixes (not necessarily whole words)
# note: most of the whole-word substitutions could be moved to the us-english-ivona.lex file, if using ivona
d_replace = {
  # any (mostly to fix wrong ivona word splittings)
  'aerodynamic': 'aero-dynamic',
  'afterhour': 'after-hour',
  'alleyway': 'alley-way',
  'allfather': 'all-father',
  'asshat': 'ass-hat',
  'bannerman': 'banner-man',
  'bannermen': 'banner-men',
  'battleaxe': 'battle-axe',
  'battlecry': 'battle-cry',
  'bedrock': 'bed-rock',
  'belowdeck': 'below-deck',
  'bioweapon': 'bio-weapon',
  'breastplate': 'breast-plate',
  'businessman': 'business-man',
  'businessmen': 'business-men',
  'busywork': 'busy-work',
  'cliffnotes': 'cliff-notes',
  'coffeemaker': 'coffee-maker',
  'coldblood': 'cold-blood',
  'coldheart': 'cold-heart',
  'conman': 'con-man',
  'conmen': 'con-men',
  'counterattack': 'counter-attack',
  'dickhead': 'dick-head',
  'disappear': 'dis-appear',
  'dungheap': 'dung-heap',
  'dynamite': 'dyna-mite',
  'fleabag': 'flea-bag',
  'forestall': 'fore-stall',
  'formalwear': 'formal-wear',
  'formfit': 'form-fit',
  'furiously': """<phoneme alphabet='x-sampa' ph='fjUri@sli'/>""", # default: ~ 'furryous'
  'furious': """<phoneme alphabet='x-sampa' ph='fjUri@s'/>""", # default: ~ 'furryous'
  'fury': """<phoneme alphabet='x-sampa' ph='fjU@ri'/>""", # default: ~ 'furry' (sometimes)
  'gateway': 'gate way',
  'goddamn': 'god-damn',
  'gruesome': 'grue-some',
  'happenstance': 'happen-stance',
  'heartache': 'heart-ache',
  'hedgerow': 'hedge-row',
  'hungover': 'hung-over',
  'jinglebell': 'jingle-bell',
  'keratin': """<phoneme alphabet='x-sampa' ph="'ker@tIn"/>""",
  'kinsman': 'kins-man',
  'kinsmen': 'kins-men',
  'kinswoman': 'kins-woman',
  'kinswomen': 'kins-women',
  'knucklehead': 'knuckle-head',
  'lethalness': 'lethal-ness',
  'levelheaded': 'level-headed',
  'lumberyard': 'lumber-yard',
  'marvelous': """<phoneme alphabet='x-sampa' ph="'mArv@l@s'"/>""",
  'megadeath': 'mega-death',
  'melodrama': 'melo-drama',
  'middleage': 'middle-age',
  'moreso': 'more-so',
  'movies': """<phoneme alphabet='x-sampa' ph="mu:vis'"/>""",
  'movie': """<phoneme alphabet='x-sampa' ph="mu:vi"/>""",
  'nobleman': 'noble-man',
  'outlaw': 'out-law',
  'outnumber': 'out-number',
  'patina': """<phoneme alphabet='x-sampa' ph="'pat@n@"/>""",
  'photosphere': 'photo-sphere',
  'pilgrimage': """<phoneme alphabet='x-sampa' ph="ˈpilgr@midZ"/>""",
  'pocketwatch': 'pocket-watch',
  'polearm': 'pole-arm',
  'renegotiate': 're-negotiate',
  'requirement': "require'ment",
  'rooftop': 'roof-top',
  'sickhouse': 'sick-house',
  'singsong': 'sing-song',
  'someone': 'some-one',
  'soundcheck': 'sound-check',
  'soundproof': 'sound-proof',
  'stableyard': 'stable yard',
  'storeroom': 'store-room',
  'sublevel': 'sub-level',
  'superweapon': 'super-weapon',
  'sweetheart': 'sweet-heart',
  'teardrop': 'tear-drop',
  'toenail': 'toe-nail',
  'troublesome': 'trouble-some',
  'unreadable': 'un-readable',
  'widescreen': 'wide-screen',
  'wyrd': 'word',
  'wyrm': 'worm',

  # saint vs street
  'st francis': 'saint francis',
  'st. francis': 'saint francis',
  'st james': 'saint james',
  'st. james': 'saint james',
  'st john': 'saint john',
  'st. john': 'saint john',
  'st louis': 'saint louis',
  'st. louis': 'saint louis',
  'st paul': 'saint paul',
  'st. paul': 'saint paul',
  'st peter': 'saint peter',
  'st. peter': 'saint peter',
}

# ivona: replace whole words (acronyms)
d_replace_whole = {
  'ceo': 'C E O',
  'ceos': "C E O's",
  'cgi': 'C G I',
  'csi': 'C S I',
  'ct': 'C T',
  'dvds': "D V D's",
  'emp': 'E M P',
  'emt': 'E M T',
  'emts': "E M T's",
  'id': 'I D',
  'ids': "I D's",
  'isp': 'I S P',
  'mre': 'M R E',
  'mres': "M R E's",
  'mri': 'M R I',
  'omg': 'O M G',
  'pda': 'P D Ae',
  'pdas': "P D Ae's",
  'pe': 'P E',
  'pms': 'P M S ',
  'pta': 'P T Ae',
  'suvs': "S U V's",
  'tmi': 'T M I',
}

# replace whole words (after everything else)
d_replace_words_after_re = {
  'g': 'gee', # default: 'grams'
  'h': 'aitch', # default: <number> 'hours'
  'm': 'em', # default: meters
}


def prepare_line(line):
  for s in l_space_before_punctuation:
    r = r'\b{}([{}])'.format(s, string.punctuation)
    line = re.sub(r, r'{} \1'.format(s), line)
  
  for key, val in d_replace_words_re.items():
    r = r'\b{}\b'.format(key)
    line = re.sub(r, val, line)
  
  for key, val in d_replace.items():
    line = line.replace(key, val)
  
  for key, val in d_replace_whole.items():
    r = r'\b{}\b'.format(key)
    line = re.sub(r, val, line)
  
  for key, val in d_replace_words_after_re.items():
    r = r'\b{}\b'.format(key)
    line = re.sub(r, val, line)

  return line



# http://developer.ivona.com/en/ttsresources/text_normalization/text_normalization_en.html
# http://developer.ivona.com/en/ttsresources/ssml/ssml.html
# http://developer.ivona.com/en/ttsresources/phonesets/phoneset-en_us.html
