#!/usr/bin/env python3

import os
import string
import subprocess
import sys
import termios
import _thread
import time
import tty


# global vars
g_keypresses = []
g_dictionaries = [
  "Merriam-Webster's Collegiate 11th Ed. (En-En)",
  'Longman Dictionary of Contemporary English 5th Ed. (En-En)',
  "Webster's Third New International Dictionary, Unabridged (En-En)",
  "Merriam-Webster's Advanced Learner's Dictionary (En-En)",
  "Oxford Advanced Learner's Dictionary 8th Ed.",
  'Cambridge Advanced Learners Dictionary 3th Ed. (En-En)',
  "WordNet® 3.0 (En-En)",
  "Roget's II The New Thesaurus 3th Ed. (En-En)",
  'Cambridge Dictionary of American Idioms (En-En)',
  'American_Idioms 2nd Ed',
  'Jargon File',
  'Urban Dictionary P1 (En-En)',
  'Urban Dictionary P2 (En-En)',
]
g_current_dic = 0
g_sleep_time = .1


def main():
  print_help()
  
  # watch for keypresses, non-blocking
  setup_term()
  _thread.start_new_thread(watch_keyboard_loop, ())
  
  # watch for selected text, non-blocking
  _thread.start_new_thread(watch_selected_loop, ())
  
  while True:
    c = getch()
    process_input(c)
    time.sleep(g_sleep_time)
    

def print_help():
  print()
  print('select text on any window to start a search')
  print()
  print('keys:')
  print('  ↓: search next dictionary')
  print('  ↑: search previous dictionary')
  print('  c: clear the screen')
  print('  q: quit')
  print()


def process_input(key):
  if not key:
    return
  
  #print(ord(key), key)
  
  # Q -> quit
  if key in 'qQ':
    reset_term()
    sys.exit()

  # C -> clear the screen
  if key in 'cC':
    clear_screen()
  
  # up arrow -> search selection in prev dictionary
  elif ord(key) == 65:
    search_prev()
  
  # down arrow -> search selection in next dictionary
  elif ord(key) == 66:
    search_next()
 

def search_prev():
  global g_current_dic
  g_current_dic -= 1
  if g_current_dic < 0:
    g_current_dic = 0
    return
  search_selected()


def search_next():
  global g_current_dic
  g_current_dic += 1
  if g_current_dic >= len(g_dictionaries):
    g_current_dic = len(g_dictionaries) - 1
    return
  search_selected()


def prepare_text(text):
  text = trim_punctuation(text).lower()
  
  #print(text)
  return text


def trim_punctuation(text):
  return text.replace('\n', ' ').strip(string.punctuation).strip()


def watch_selected_loop():
  global g_current_dic
  while True:
    p = subprocess.Popen(['clipnotify'])
    retcode = p.wait()
    g_current_dic = 0
    search_selected()
    time.sleep(g_sleep_time)


def search_selected():
  selected = get_selected_text()
  prepared_text = prepare_text(selected)
  definition = search_in_current_dic(prepared_text)
  show_definition(definition)
  
  
def show_definition(text):
  clear_screen()
  dic = get_current_dictionary()
  reset_term()
  print(f'using dictionary: {dic}')
  print(text)
  setup_term()


def get_current_dictionary():
  return g_dictionaries[g_current_dic]


def get_selected_text():
  p = subprocess.Popen(['xsel'], stdout = subprocess.PIPE)
  retcode = p.wait()
  data = p.stdout.read()
  return data.decode()


def search_in_current_dic(text):
  dic = get_current_dictionary()
  p = subprocess.Popen([ 'sdcv', '--non-interactive', '--exact-search', '-u', dic, text ], stdout = subprocess.PIPE)
  retcode = p.wait()
  data = p.stdout.read()
  return data.decode()


def watch_keyboard_loop():
  while True:
    watch_keyboard()
    time.sleep(g_sleep_time)


def watch_keyboard():
  global g_keypresses

  try:
    key = read_key()
    while ord(key) in (27, 79, 91):
      key = read_key()
    
    g_keypresses.append(key)

  except Exception as e:
    print(e)


def read_key():
  k = sys.stdin.read(1)
  return k


def setup_term():
  global g_fd
  global g_old_settings

  g_fd = sys.stdin.fileno()
  g_old_settings = termios.tcgetattr(g_fd)
  tty.setraw(sys.stdin.fileno())


def reset_term():
  termios.tcsetattr(g_fd, termios.TCSADRAIN, g_old_settings)


def clear_screen():
  os.system('clear')


def getch():
  global g_keypresses
  
  if not kbhit():
    return None

  ch = g_keypresses.pop(0)

  return ch


def kbhit():
  return g_keypresses != []


if __name__ == '__main__':
  main()
